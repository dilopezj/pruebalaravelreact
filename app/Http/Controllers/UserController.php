<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\User;
use DB;

/**
 * Description of UserController
 *
 * @author donna.lopez
 */
class UserController extends Controller {

    //put your code here
    #Funciones de Visualizacion de vistas (views)

    /*     * Vista Creacion de Usuario** */

    public function Users() {
        return view('user');
    }

    public function getUser_all() {
        return User::all();
    }

    public function createUser(Request $request) {
        $input = $request->all();
        $duplicate = 0;
        $response['message'] = "";

        DB::beginTransaction();
        try {

            if ($crud = User::where('identification', $input['identification'])->first()) {
                $duplicate = 1;
                $response['message'] .= "* Esta cédula ya fue ingresada al sistema con otro usuario. \n";
                $response['succes'] = false;
            }
            if ($crud = User::where('email', $input['email'])->first()) {
                $duplicate = 1;
                $response['message'] .= "* Esta correo ya fue ingresado al sistema con otro usuario. \n";
                $response['succes'] = false;
            }

            if ($duplicate == 0) {
                $insert = ['name' => $input['name'], 'lastname' => $input['lastname'], 'email' => $input['email'], 'phone' => $input['phone'], 'identification' => $input['identification']];
                $insertUser = User::insert($insert);
                $response['message'] = "Datos guardados exitosamente.";
                $response['succes'] = true;
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $response['message'] = $e->getMessage();
            $response['succes'] = false;
        }

        return $response;
    }

    public function updateUser(Request $request) {
        $input = $request->all();
        $duplicate = 0;
        $response['message'] = "";

        DB::beginTransaction();
        try {

            if ($crud = User::where('identification', $input['identification'])->where('id', '!=', $input['id'])->first()) {
                $duplicate = 1;
                $response['message'] .= "* Esta cédula ya fue ingresada al sistema con otro usuario. \n";
                $response['succes'] = false;
            }
            if ($crud = User::where('email', $input['email'])->where('id', '!=', $input['id'])->first()) {
                $duplicate = 1;
                $response['message'] .= "* Esta correo ya fue ingresado al sistema con otro usuario. \n";
                $response['succes'] = false;
            }

            if ($duplicate == 0) {
                $update = ['name' => $input['name'], 'lastname' => $input['lastname'], 'email' => $input['email'], 'phone' => $input['phone'], 'identification' => $input['identification']];
                User::where('id', $input['id'])->update($update);
                $response['message'] = "Datos editados exitosamente.";
                $response['succes'] = true;
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $response['message'] = $e->getMessage();
            $response['succes'] = false;
        }

        return $response;
    }

}
