<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//----Panel Crear Usuarios ---
Route::post('/user', 'App\Http\Controllers\UserController@Users');
Route::get('user/list','App\Http\Controllers\UserController@getUser_all');
Route::post('user/create', 'App\Http\Controllers\UserController@createUser');
Route::post('user/update', 'App\Http\Controllers\UserController@updateUser');