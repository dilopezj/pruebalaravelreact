import React, { Component } from 'react';
import ReactDOM from 'react-dom';      

   export default class CreateUser extends Component {
        constructor(props){
        super(props);
                this.state = {name:'',lastname:'',email:'',phone:'',identification:''}        
                
                this.handleChangeName = this.handleChangeName.bind(this);
                this.handleChangeLastname = this.handleChangeLastname.bind(this);
                this.handleChangeEmail = this.handleChangeEmail.bind(this);
                this.handleChangePhone = this.handleChangePhone.bind(this);
                this.handleChangeIdentification = this.handleChangeIdentification.bind(this);
        }       
                    
          
        render() {
            return (
                    <div className="container mt-5">
                        <div className="row justify-content-center">
                            <div className="col-md-12">
                                <div className="card text-center">
                                    <div className="card-header"><h2>Crear Usuarios</h2></div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group">
                                            <label className="label" htmlFor="identification">Cédula: </label><br></br>
                                            <input autoFocus required placeholder="Cédula" type="text" id="identification" onChange={this.handleChangeIdentification} value={this.state.identification} className="input solo-numero number" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="name">Nombres: </label><br></br>
                                            <input autoFocus required placeholder="Nombre" type="text" id="name" onChange={this.handleChangeName} value={this.state.name} className="input" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="lastname">Apellidos: </label><br></br>
                                            <input autoFocus required placeholder="Apellidos" type="text" id="lastname" onChange={this.handleChangeLastname} value={this.state.lastname} className="input" />
                                        </div> 
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="email">Correo:</label><br></br>
                                            <input required placeholder="Correo" type="email" id="email" onChange={this.handleChangeEmail} value={this.state.email} className="input" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="phone">Teléfono:</label><br></br>
                                            <input required placeholder="Teléfono" type="text" id="phone" onChange={this.handleChangePhone} value={this.state.phone} className="input solo-numero number" />
                                        </div>
                                    </div>
                                </div>                     
                                    <div className="form-group">
                                        <button className="button is-success mt-2 btn btn-success" onClick={()=>this.sendForms()}>Guardar</button>
                                    </div>
                            </div>
                        </div>
                    </div>                
            );
        }
        
        handleChangeName(event) {            
            this.setState({name: event.target.value});
        }
        
        handleChangeLastname(event) {
            this.setState({lastname: event.target.value});
        }
        
        handleChangeEmail(event) {
            this.setState({email: event.target.value});
        }
        
        handleChangePhone(event) {
            const { name, value } = event.target;
            let regex = new RegExp(/^[0-9\s]+$/g);
            if (regex.test(value)) {
              this.setState({phone: event.target.value});         
            }           
        }
        
        handleChangeIdentification(event) {
            const { name, value } = event.target;
            let regex = new RegExp(/^[0-9\s]+$/g);
            if (regex.test(value)) {
              this.setState({identification: event.target.value});     
            }              
        }
        
        validEmail(value) {
            let regex = new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
            if (regex.test(value)) {
              return true   
            }else{
               return false
            }              
        }
                 
        sendForms() {
            const formData = new FormData()
            formData.append('identification',this.state.identification)
            formData.append('name',this.state.name)
            formData.append('lastname',this.state.lastname)
            formData.append('email',this.state.email)
            formData.append('phone',this.state.phone) 
            
            if(this.validEmail(this.state.email) == true){
                axios.post('user/create',formData).then(response=>{ 
                   alert(response.data.message) 
                   if(response.data.succes == true){
                     this.loadListUser();
                    }
                }).catch(error=>{
                  alert("Error "+error)
                })
            }else{
              alert("Formato de correo invalido") 
            }
        }        
    }
    
// DOM element
if (document.getElementById('createUser')) {
    ReactDOM.render(<CreateUser />, document.getElementById('createUser'));
}


