import React, { Component } from 'react';
import ReactDOM from 'react-dom';      

   export default class User extends Component {
        constructor(props){
        super(props);
                this.state = {
                user:[],
                id:0,
                name:'',lastname:'',email:'',phone:'',identification:'',edit:false
                }
                this.handleChangeName = this.handleChangeName.bind(this);
                this.handleChangeLastname = this.handleChangeLastname.bind(this);
                this.handleChangeEmail = this.handleChangeEmail.bind(this);
                this.handleChangePhone = this.handleChangePhone.bind(this);
                this.handleChangeIdentification = this.handleChangeIdentification.bind(this);
        }
        
        componentDidMount(){
           this.loadListUser();
        }
        
        loadListUser(){
            axios.get('user/list').then(response => {
            this.setState({user:response.data})
            }).catch(error => {alert("Error " + error)})
        }
                
        render() {
            return (
                    <div className="container mt-5">
                        {this.renderCreate()}
                        <div className="row justify-content-center p-2">
                            <div className="col-md-12">
                                <div class="card">                                
                                <div class="card-body">
                                <h5 class="card-title">Tabla de Usuarios</h5>
                                <table className="table table-bordered order-table ">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>Cédula</th>
                                            <th>Correo</th>
                                            <th>Teléfono</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodytable">
                                      {this.renderList()}
                                    </tbody>
                                </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    );
        }
        
        renderCreate(){ 
            return (
                        <div className="row justify-content-center p-2">
                            <div className="col-md-12">
                                <div className="card text-center">
                                    <div className="card-header "><h2>Gestión de usuarios</h2></div>
                                </div>                                
                                <div class="card">
                                <div class="card-body">
                                <h5 class="card-title">Formulario de Usuarios</h5>
                                <div className="row p-4">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="identification">Cédula: </label><br></br>
                                            <input autoFocus required placeholder="Cédula" type="text" id="identification" onChange={this.handleChangeIdentification} value={this.state.identification} className="form-control input number" maxLength="12" />
                                        </div>
                                    </div>
                                    <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="name">Nombres: </label><br></br>
                                            <input autoFocus required placeholder="Nombre" type="text" id="name" onChange={this.handleChangeName} value={this.state.name} className="form-control input" maxLength="200" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="lastname">Apellidos: </label><br></br>
                                            <input autoFocus required placeholder="Apellidos" type="text" id="lastname" onChange={this.handleChangeLastname} value={this.state.lastname} className="form-control input" maxLength="200" />
                                        </div> 
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="email">Correo:</label><br></br>
                                            <input required placeholder="Correo" type="email" id="email" onChange={this.handleChangeEmail} value={this.state.email} className="form-control input" maxLength="200" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label className="label" htmlFor="phone">Teléfono:</label><br></br>
                                            <input required placeholder="Teléfono" type="text" id="phone" onChange={this.handleChangePhone} value={this.state.phone} className="form-control input number" maxLength="11" />
                                        </div>
                                    </div>
                                </div>   
                                <div className="row">
                                    <div className="col-md-6"></div>
                                    <div className="col-md-6">
                                        <div className="form-group text-right">
                                            <button type="button" className="button is-secondary mt-2 mr-2 btn btn-secondary" onClick={()=>this.cleanUser()}>Cancelar</button>
                                            <button className="button is-success mt-2 btn btn-success" onClick={()=>this.sendOption()}>Guardar</button>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>           
            );
        }
                        
        renderList(){
            return this.state.user.map((data)=>{
              return(
                <tr>
                  <td>{data.id}</td>
                  <td>{data.name}</td>
                  <td>{data.lastname}</td>
                  <td>{data.identification}</td>
                  <td>{data.email}</td>
                  <td>{data.phone}</td>
                  <td><button className="btn btn-info text-white" onClick={()=>this.showEdit(data)}>Editar</button></td>
                </tr>
              )
            })
         }
         
         showEdit(data){
            //alert("mostrar modal "+JSON.stringify(data))
            this.setState({
              id:data.id,
              name:data.name,
              lastname:data.lastname,
              identification: data.identification,
              email: data.email,
              phone: data.phone,
              edit:true
            })
          }
          
          handleChangeName(event) {            
            this.setState({name: event.target.value});
        }
        
        handleChangeLastname(event) {
            this.setState({lastname: event.target.value});
        }
        
        handleChangeEmail(event) {
            this.setState({email: event.target.value});
        }
        
        handleChangePhone(event) {
            const { name, value } = event.target;
            let regex = new RegExp(/^[0-9\s]+$/g);
            if (regex.test(value)) {
              this.setState({phone: event.target.value});         
            }           
        }
        
        handleChangeIdentification(event) {
            const { name, value } = event.target;
            let regex = new RegExp(/^[0-9\s]+$/g);
            if (regex.test(value)) {
              this.setState({identification: event.target.value});     
            }              
        }
        
        validEmail(value) {
            let regex = new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
            if (regex.test(value)) {
              return true   
            }else{
               return false
            }              
        }
        cleanUser() {
            this.setState({
              name:'',
              lastname:'',
              identification: '',
              email: '',
              phone: '',
              edit:false
            })
        }
        
        sendForms() {
            const formData = new FormData()
            formData.append('identification',this.state.identification)
            formData.append('name',this.state.name)
            formData.append('lastname',this.state.lastname)
            formData.append('email',this.state.email)
            formData.append('phone',this.state.phone) 
            
            if(this.validEmail(this.state.email) == true){
                axios.post('user/create',formData).then(response=>{ 
                   alert(response.data.message) 
                   if(response.data.succes == true){
                     this.loadListUser();
                     this.cleanUser();
                    }
                }).catch(error=>{
                  alert("Error "+error)
                })
            }else{
              alert("Formato de correo invalido") 
            }
        }     
                 
        sendUpdateUser() {
            const formData = new FormData()
            formData.append('identification',this.state.identification)
            formData.append('name',this.state.name)
            formData.append('lastname',this.state.lastname)
            formData.append('email',this.state.email)
            formData.append('phone',this.state.phone) 
            formData.append('id',this.state.id) 
            
            if(this.validEmail(this.state.email) == true){
                axios.post('user/update',formData).then(response=>{ 
                   alert(response.data.message)  
                   if(response.data.succes == true){
                     $("#exampleModal").modal("hide");
                     this.loadListUser();
                     this.cleanUser();
                    }
                }).catch(error=>{
                  alert("Error "+error)
                })
            }else{
              alert("Formato de correo invalido") 
            }
        }  
        
         sendOption() {
            if(this.state.edit == true){
                this.sendUpdateUser();
            }else{
                this.sendForms();
            }
         }
    }
    
// DOM element
if (document.getElementById('user')) {
    ReactDOM.render(<User />, document.getElementById('user'));
}


